package com.example.rps;

public enum Move {
    ROCK {
        @Override
        public boolean wins_over(Move opposed) {
            return opposed == SCISSOR;
        }

    },
    PAPER {
        @Override
        public boolean wins_over(Move opposed) {
            return opposed == ROCK;
        }
    },
    SCISSOR {
        @Override
        public boolean wins_over(Move opposed) {
            return opposed == PAPER;
        }
    };


    public abstract boolean wins_over(Move opposed);

    public static Boolean validMove(String turn) {
        return turn != null;
    }


}
