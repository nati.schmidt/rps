package com.example.rps;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.UUID;

//firstly building the class with properties


@Entity
public class Game {
@Id
    private UUID uuid;
    private String userName;
    private Move playerMove;
    private Gamestatus gamestatus;
    private String opponentName;
    private Move opponentMove;

    //Constructor
    public Game(UUID uuid,
                String userName,
                Move playerMove,
                Gamestatus gamestatus,
                String opponentName,
                Move opponentMove) {
        this.uuid = uuid;
        this.userName = userName;
        this.playerMove = playerMove;
        this.gamestatus = gamestatus;
        this.opponentName = opponentName;
        this.opponentMove = opponentMove;
    }

    //Getters and setters
    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Move getPlayerMove() {
        return playerMove;
    }

    public void setPlayerMove(Move playerMove) {
        this.playerMove = playerMove;
    }

    public Gamestatus getGamestatus() {
        return gamestatus;
    }

    public void setGamestatus(Gamestatus gamestatus) {
        this.gamestatus = gamestatus;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public Move getOpponentMove() {
        return opponentMove;
    }

    public void setOpponentMove(Move opponentMove) {
        this.opponentMove = opponentMove;
    }



}
